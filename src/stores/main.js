function unmutatedArr(a){
    let newArr = []
    for(let i = 0; i < a.length; i++){
        //let copiedArr = state.store[i].slice()
        let copiedArrEl = [...a[i]]
        newArr.push(copiedArrEl)
    }
    return newArr
}
function sortUpNewArr(a, s){
    return a.sort((a,b) => a[s] - b[s])
}
function sortDownNewArr(a,s){
    return a.sort((a,b) => a[s] == b[s] ? 0 : a[s] > b[s] ? -1 : 1)
}
function newId(e){
    let newArr = sortDownNewArr(e, 0)
    return newArr[0][0] + 1
}
export default {
    strict: true,
    state: {
        store: [
            [1, "iPhone 5", 900, 5 ],
            [2, "XBOX", 300, 7 ],
            [3, 'switch', 500, 1]
        ],
        newStoreString: ''
    },
    mutations: {
        deleteLine(state, param){
            let arr = unmutatedArr(state.store)
            if(arr.length == 1){
                state.store = []
            }
            else{
                for(let i = 0; i < arr.length; i++){
                    if(arr[i][0] == param.line){
                        delete arr[i]
                    }
                }
                let newArr = arr.filter(a=>a)
                state.store = newArr
            }
        },
        setNewLine(state){
            let newString = state.newStoreString.split(',')
            if(state.store.length == 0){
                newString.unshift(1)
                state.store = [newString]
                state.newStoreString = ''
            }
            else{
            let arr = unmutatedArr(state.store)
            let mutableArr = unmutatedArr(arr)
            let id = newId(mutableArr)
            newString.unshift(id)
            arr.push(newString)
            state.store = arr
            state.newStoreString =""
            }
        },
        newLineString(state, param){
            state.newStoreString = param.text
        },
        sort(state, param){
            if(param.id == 0 || param.id == 2  || param.id == 3 ){
            let newArr = unmutatedArr(state.store)
                if(param.sortType == 'up'){
                    state.store = sortUpNewArr(newArr, param.id)
                }
                else if(param.sortType == 'down'){
                    state.store = sortDownNewArr(newArr, param.id)
                }
            }
            else{
            let newArr = unmutatedArr(state.store)
            if(param.sortType == 'up'){
                newArr.sort((a,b) => a[1].toLowerCase() == b[1].toLowerCase() ? 0 : a[1].toLowerCase() > b[1].toLowerCase() ? 1 : -1)
                state.store = newArr
            }
            else if(param.sortType == 'down'){
                newArr.sort((a,b) => a[1].toLowerCase() == b[1].toLowerCase() ? 0 : a[1].toLowerCase() > b[1].toLowerCase() ? -1 : 1)
                state.store = newArr
            }
        }
        }
    },
    actions: {

    },
    getters:{
        getStore(state){
            return state.store
        },
        getNewString(state){
            return state.newStoreString
        }
    }
}