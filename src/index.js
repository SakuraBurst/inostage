import './styles/style.css';
import App from './App.vue'

window.Vue = require("@vue")

import store from './stores/index'

new Vue({
    store,
    render: h=> h(App)
}).$mount('#app')
